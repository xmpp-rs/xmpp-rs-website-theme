# Unreleased

Notables changes since upstream 5c698271c460046034605b743a15196b12e32887

- a `helpers::link(dest=URL)` macro is now available to help with internal/external links
- index page (`content/_index.md`) `extra.url` can link to external sites
- index page can have an image above main title, with `section.extra.title_img` that will occupy maximum `section.extra.title_img_lines` lines (default 3)
- site header `extra.menu.main` entries can link to external sites
- use of light/dark theme is enforced by browser properties (`prefers-color-scheme`), not by button